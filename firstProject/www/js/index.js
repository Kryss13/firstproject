const divTopTitan = `
<div class="wrapper mt-4">
    <div class="titan-card titan" id="titan">
        <div class="titan-card__image titan-card__image--titan">
            <img src="§source§" alt="titan" />
        </div>
        <div class="titan-card__unit-name">§title§</div>
        <div class="titan-card__unit-description">
            §description§
        </div>

        <div class="titan-card__unit-stats titan-card__unit-stats--titan clearfix">
            <div class="one-third no-border">
            <div class="stat-value">Taille</div>
            <div class="stat">§height§ mètres</div>
            </div>

        </div>
    </div>
</div>`;

const imageSource = [
    { source:'./img/titan/eren.png'},
    { source:'./img/titan/beast.png'},
    { source:'./img/titan/cart.png'},
    { source:'./img/titan/colossal.png'},
    { source:'./img/titan/armored.png'},
    { source:'./img/titan/female.png'},
    { source:'./img/titan/jaw.png'},
    { source:'./img/titan/originel.png'},
    { source:'./img/titan/warHammer.png'},
    { source:'./img/titan/ymir.png'},
    { source:'./img/titan/abnormal.png'},
    { source:'./img/titan/armored_2.png'},
    { source:'./img/titan/beast_2.png'},
    { source:'./img/titan/colossal_2.png'},
    { source:'./img/titan/female_2.png'},
    { source:'./img/titan/pure.png'},
    { source:'./img/titan/titan_1.png'},
    { source:'./img/titan/titan_2.png'},
    { source:'./img/titan/titan_3.png'},
    { source:'./img/titan/titan_4.png'},
    { source:'./img/titan/wallTitan.png'},
]

document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {
    console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);

    navigator.geolocation.getCurrentPosition(onSuccessGetCurrentPosition, onErrorGetCurrentPosition);

    // Lock orientation to portrait
    screen.orientation.lock('portrait');

    $("#plusButton").click(function () {
        $("#addPage, #myTopPage").toggle("hidden");
    });

    $("#home ,#myTop").click(function () {
        $("#homePage, #myTopPage").toggle("hidden");
        fillDivTitan();
    });


    $("#submitTitan").click(function (event) {
        // Get all titans store in local storage
        let allTitans = localStorage.getItem('titans');
        if (allTitans == null) {
            // No titan store 
            allTitans = [];
        } else {
            // Parse to get a array to pass the length in id
            allTitans = JSON.parse(allTitans);
        }

        // Get values from form
        const name = $.trim($('#name').val());
        const description = $.trim($('#description').val());
        const height = $.trim($('#height').val());
        const imageIdSelected = $('#image').val();

        // If random image is chosen, we do a random, otherwise we choose the corresponding image
        let imageId;
        if (imageIdSelected == 10) {
            imageId = Math.floor(Math.random() * 14);
        }
        else{
            imageId = imageIdSelected
        }
        // Create object
        const titan = {
            id: allTitans.length,
            source : imageSource[imageId].source,
            name: name,
            description: description,
            height : height
        }

        // Push new titan in array
        allTitans.push(titan);

        // Set all titans in local storage
        window.localStorage.setItem("titans", JSON.stringify(allTitans));

        // Dialogs plugin
        navigator.notification.alert(
            'Vous avez bien ajouté le titan ' + titan.name,  // Message
            doSomething,                                     // Callback
            '/!\ FÉLICITATION /!\\',                         // Title
            'OK'                                             // Button name
        );
    });
}
function fillDivTitan() {
    // Get all titans store in local storage
    let allTitans = localStorage.getItem('titans');
    allTitans = JSON.parse(allTitans);

    // Remove div to avoid duplicate
    if (allTitans != null) {
        $("#listMyTop div").remove();
    }

    let divListMyTop = $("#listMyTop");
    // Replace to add new titan
    $.each(allTitans, function(index, obj){
        const newDivTitan = divTopTitan
        .replace("§source§", obj.source)
        .replace("§title§", obj.name)
        .replace("§description§", obj.description)
        .replace("§height§", obj.height)
        divListMyTop[0].appendChild(htmlToElement(newDivTitan));
    }) 
}
function doSomething(){
    console.log("clic sur le bouton du navigator.notification.alert !");
}

/*** POSITION ***/
// Method on sucess to load position
var onSuccessGetCurrentPosition = function(position) {
    // When success to get position, we launch method to get the weather
    onWeatherSuccess(position);
};

// onError Callback receives a PositionError object
function onErrorGetCurrentPosition(error) {
    alert('code: '    + error.code    + '\n' +
            'message: ' + error.message + '\n');
}

/*** WEATHER ***/ 
// Success callback for get geo coordinates
var onWeatherSuccess = function (position) {

    Latitude = position.coords.latitude;
    Longitude = position.coords.longitude;

    getWeather(Latitude, Longitude);
}

// Get weather by using coordinates
function getWeather(latitude, longitude) {
    // My key on Open Weather Map
    var OpenWeatherAppKey = "274783d1f275b807c48bd134577fb219";

    var queryString =
      'http://api.openweathermap.org/data/2.5/weather?lat='
      + latitude + '&lon=' + longitude + '&appid=' + OpenWeatherAppKey + '&units=imperial&lang=fr';

    $.getJSON(queryString, function (results) {

        if (results.weather.length) {

            $.getJSON(queryString, function (results) {

                if (results.weather.length) {

                    let description = results.name;
                    let tempFahrenheit = results.main.temp;
                    let wind = results.wind.speed;
                    let humidity = results.main.humidity;
                    let visibility = results.weather[0].main;

                    let tempCelcius = convertToCelsius(tempFahrenheit);

                    navigator.notification.alert(
                        'Ville : ' + description  + '\n' +
                        'Température : ' + tempCelcius + ' °C\n' +
                        'Vent : '  + wind + ' mètre/sec\n' +
                        'Humidité : ' + humidity + ' %\n' +
                        'Visibilité : ' + visibility + '\n', // Message
                        doSomething,                         // Callback
                        '/!\ MÉTÉO DE CHEZ VOUS /!\\',       // Title
                        'OK'                                 // Button name
                    );
                }

            });
        }
    }).fail(function () {
        console.log("error getting location");
    });
}

// Error callback
function onWeatherError(error) {
    console.log('code: ' + error.code + '\n' +
        'message: ' + error.message + '\n');
}

function convertToCelsius(fahrenheit){
    let result = (fahrenheit - 32) / 1.8;
    return result.toFixed(2);
}

/*** BATTERY STATUS ***/
function onBatteryStatus(status) {
    console.log("Niveau de batterie : " + status.level + " %" + "\n" + 
    "En charge : " + status.isPlugged
    );
}