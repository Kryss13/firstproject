const divTitan = `
<div class="wrapper mt-4">
    <div class="titan-card titan" id="titan">
        <div class="titan-card__image titan-card__image--titan">
            <img src="§source§" alt="titan" onclick="openInAppBrowser('https://fr.wikipedia.org/wiki/L%27Attaque_des_Titans')"/>
        </div>
        <div class="titan-card__unit-name">§title§</div>
        <div class="titan-card__unit-description">
            §description§
        </div>

        <div class="titan-card__unit-stats titan-card__unit-stats--titan clearfix">
            <div class="one-third">
            <div class="stat-value">Nom</div>
            <div class="stat">§name§</div>
            </div>

            <div class="one-third no-border">
            <div class="stat-value">Taille</div>
            <div class="stat">§height§</div>
            </div>

        </div>
    </div>
</div>`;

const htmlToElement = (html) => {
    const template = document.createElement("template");
    html = html.trim(); // Never return a text node of whitespace as the result
    template.innerHTML = html;
    return template.content.firstChild;
};

const fetchApiDone = (json) => {
    const divList = document.getElementById("list");
    json.forEach((titan) => {
        const newDivTitan = divTitan
            .replace("§source§", titan.img)
            .replace("§title§", titan.title)
            .replace("§description§", titan.description)
            .replace("§name§", titan.name)
            .replace("§height§", titan.height);
        divList.appendChild(htmlToElement(newDivTitan));
    });
};

document.addEventListener("DOMContentLoaded", () => {
    fetchLocal("api/titan.json").then((response) =>
        response.json().then(fetchApiDone)
    );
});

function fetchLocal(url) {
    return new Promise(function(resolve, reject) {
      var xhr = new XMLHttpRequest
      xhr.onload = function() {
        resolve(new Response(xhr.responseText, {status: xhr.status}))
      }
      xhr.onerror = function() {
        reject(new TypeError('Local request failed'))
      }
      xhr.open('GET', url)
      xhr.send(null)
    })
  }

// In app browser
function openInAppBrowser(url) {
    var options = "location=yes,zoom=false";
    cordova.InAppBrowser.open(url, "_blank", options);    
}