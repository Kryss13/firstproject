<a href="https://fr.wikipedia.org/wiki/L%27Attaque_des_Titans">
    <img src="https://www.pngmart.com/files/13/Attack-On-Titan-Word-Logo-Transparent-PNG.png" alt="snk logo" title="SNK" align="right" height="45" />
</a>

# Top Shingeki no Kyojin 進撃の巨人 

[![Awesome](https://cdn.rawgit.com/sindresorhus/awesome/d7305f38d29fed78fa85652e3a63e154dd8e8829/media/badge.svg)](https://www.youtube.com/watch?v=dQw4w9WgXcQ)

:star: The best Attack on titan top ever !

# Getting started

## Built With

* [Cordova](https://cordova.apache.org/) - Mobile apps with **HTML**, **CSS** & **JS**. Target multiple platforms with **one code base**.


## Plugin used in the project
- [Screen orientation ](https://cordova.apache.org/docs/en/10.x/reference/cordova-plugin-screen-orientation/index.html) - Cordova plugin to **set/lock the screen orientation** in a common way for iOS, Android, and windows-uwp.
    - Orientation is locked in portrait mode.
- [Splash screen ](https://cordova.apache.org/docs/en/10.x/reference/cordova-plugin-splashscreen/index.html#android-specific-information) - This plugin **displays and hides a splash screen** while your web application is launching.
    - splash screen with armored titan.
- [Dialogs ](https://cordova.apache.org/docs/en/10.x/reference/cordova-plugin-dialogs/index.html) - This plugin provides access to some **native dialog UI elements** via a global *navigator.notification* object.
    - A dialog box appears when loading the application to display the weather of your home but also when adding a titan.
- [Geolocation ](https://cordova.apache.org/docs/en/10.x/reference/cordova-plugin-geolocation/index.html) - This plugin provides information about the **device's location**, such as latitude and longitude.
    - The weather is displayed in a pop-up when the application is loaded.
- [Status bar ](https://cordova.apache.org/docs/en/10.x/reference/cordova-plugin-statusbar/index.html) - The StatusBar object provides some functions to **customize the iOS and Android StatusBar**.
    - The color of the status bar is changed to match the application.
- [Battery status ](https://cordova.apache.org/docs/en/10.x/reference/cordova-plugin-battery-status/index.html) - This plugin adds the following three events to the window object: **batterystatus**, **batterycritical**, **batterylow**.
    - A simple battery status log with the level and if the device is charging.
- [In app browser](https://cordova.apache.org/docs/en/10.x/reference/cordova-plugin-inappbrowser/index.html) - You can show helpful **articles**, **videos**, and **web resources** inside of your app.
    - When clicking on an image of the nine primordial titans, Wikipedia is launched on in app browser.

All these plugins work well on Android, less on browser...


## Authors

* **Hecquet Christian** - *Software developer, mobile & iot* - [Linkedin](https://www.linkedin.com/in/christian-hecquet-978665178/)

*Did you find the easter egg in this readme ?* 😉